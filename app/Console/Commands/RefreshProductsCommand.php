<?php

namespace App\Console\Commands;

use App\Http\Clients\EmagClient;
use App\Models\Product;
use Illuminate\Console\Command;

class RefreshProductsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh all the products from eMag';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(EmagClient $client)
    {
        $page = 1;
        
        do {
            $this->info("Processing page {$page}");
            $results = $client->getLaptops($page++, 60);


            if ($results) {
                \collect($results->data->items)->each(function ($emagProduct) {
                    Product::updateOrCreate(
                        [
                            'part_number' => $emagProduct->offer->part_number,
                        ],
                        [
                            'name' => $emagProduct->name,
                            'part_number' => $emagProduct->offer->part_number,
                            'price' => $emagProduct->offer->price->current,
                            'image_url' => $emagProduct->image->original
                        ]
                    );
                });
            }
        } while ($results);
    }
}
