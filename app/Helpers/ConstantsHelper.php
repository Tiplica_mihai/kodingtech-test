<?php

define('DATE_TIME_PICKER_FORMAT_JS', 'DD-MM-YYYY HH:mm');
define('DATE_TIME_PICKER_FORMAT_PHP', 'd-m-Y H:i');

define('DATE_PICKER_FORMAT_JS', 'DD-MM-YYYY');
define('DATE_PICKER_FORMAT_PHP', 'd-m-Y');

define('FLOAT_DECIMALS', 2);

define('BLADE_LAYOUT_ADMIN', 'adminlte::page');
define('BLADE_LAYOUT_WEB', 'layouts.app');

