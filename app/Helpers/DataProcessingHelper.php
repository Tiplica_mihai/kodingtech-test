<?php

use Carbon\Carbon;

if (!function_exists('carbonFromRequest')) {

    /**
     * This function will output QR code
     *
     * @param
     * @return
     */
    function carbonFromRequest($name, $format = 'd-m-Y') {
        if (!request($name) || request($name) == '') {
            return null;
        }

        $date = Carbon::createFromFormat($format, request($name));
        return $format == 'd-m-Y' ? $date->startOfDay() : $date;
    }

}

if (!function_exists('nullOrLikeParam')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function nullOrLikeParam($value, $start = true, $end = true) {
        if ($value) {
            return ($start ? '%' : '') . $value . ($end ? '%' : '');
        }
        return $value;
    }

}

if (!function_exists('notNullValue')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function notNullValue($value, $nullValue = '-') {
        return $value ? $value : $nullValue;
    }

}


if (!function_exists('nullOrIntValue')) {

    /**
     * This function will cast the value sent to int or to a null value if not set
     *
     * @param
     * @return
     */
    function nullOrIntValue($value) {
        return $value ? (int) $value : null;
    }

}


if (!function_exists('strposa')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function strposa($haystack, $needles = array(), $offset = 0) {
        $chr = array();

        foreach ($needles as $needle) {
            $res = strpos($haystack, $needle, $offset);

            if ($res !== false)
                $chr[$needle] = $res;
        }

        if (empty($chr))
            return false;

        return min($chr);
    }

}


if (!function_exists('human_filesize')) {

    /**
     * Convert big numbers in a short human reading style
     *
     *
     * @param type $bytes
     * @param type $decimals
     * @return string
     */
    function human_filesize($bytes, $decimals = 2) {
        $size = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
    }

}

if (!function_exists('sanitize_file_name')) {

    /**
     * Remove anything which isn't a word, whitespace, number or isn't any of the following caracters -_[]().
     *
     * @param type $name
     * @return type
     */
    function sanitize_file_name($name) {
        $name = preg_replace("([^\w\s\d\-_\[\]\(\).])", '', $name);
        $name = preg_replace("([\.]{2,})", '', $name);

        return $name;
    }

}

if (!function_exists('human_int_no')) {

    /**
     * Format numbers in a more human reading style
     */
    function human_int_no($value, $thousands_separator = ' ') {
        return human_float_no($value, $thousands_separator, 0);
    }

}

if (!function_exists('human_float_no')) {

    /**
     * Format numbers in a more human reading style
     */
    function human_float_no($value, $thousands_separator = ' ', $decimals = 2, $decimal_point = '.') {
        return number_format($value, $decimals, $decimal_point, $thousands_separator);
    }

}


if (!function_exists('minified_number')) {

    /**
     * Shortens a number and attaches K, M, B, etc. accordingly
     *
     * @param
     * @return
     */
    function minified_number($number, $precision = 3, $divisors = null) {

        // Setup default $divisors if not provided
        if (!isset($divisors)) {
            $divisors = array(
                pow(1000, 0) => '', // 1000^0 == 1
                pow(1000, 1) => 'K', // Thousand
                pow(1000, 2) => 'M', // Million
                pow(1000, 3) => 'B', // Billion
                pow(1000, 4) => 'T', // Trillion
                pow(1000, 5) => 'Qa', // Quadrillion
                pow(1000, 6) => 'Qi', // Quintillion
            );
        }

        // Loop through each $divisor and find the
        // lowest amount that matches
        foreach ($divisors as $divisor => $shorthand) {
            if (abs($number) < ($divisor * 1000)) {
                // We found a match!
                break;
            }
        }

        // We found our match, or there were no matches.
        // Either way, use the last defined value for $divisor.
        return number_format($number / $divisor, $precision) . $shorthand;
    }

}

if (!function_exists('preview_text_attachment')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function preview_text_attachment($path) {
        $data = file_get_contents($path);
        $data = explode("\n", $data);

        $text = "";
        foreach ($data as $line) {
            $text .= "<span>" . htmlentities($line) . "</span>";
        }

        return $text;
    }

}

if (!function_exists('normalizeString')) {

    function normalizeStringForFolder($str = '') {
        $str = strip_tags($str);
        $str = preg_replace('/[\r\n\t ]+/', ' ', $str);
        $str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
        $str = strtolower($str);
        $str = html_entity_decode($str, ENT_QUOTES, "utf-8");
        $str = htmlentities($str, ENT_QUOTES, "utf-8");
        $str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str);
        $str = str_replace(' ', '-', $str);
        $str = rawurlencode($str);
        $str = str_replace('%', '-', $str);
        return $str;
    }

}

use DeviceDetector\DeviceDetector;

if (!function_exists('getUserAgentInfo')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function getUserAgentInfo() {
        $userAgent = request()->header('User-Agent');
        $device = new DeviceDetector($userAgent);
        $device->parse();

        return [
            'user_agent_header' => $userAgent,
            'device_client' => $device->getClient('name') . ' ' . $device->getClient('version') . ' ' . $device->getClient('engine') . '_' . $device->getClient('engine_version'),
            'device_os' => $device->getOs('name') . ' ' . $device->getOs('version'),
            'device_name' => $device->getDeviceName(),
            'device_brand' => $device->getBrandName(),
            'device_model' => $device->getModel(),
        ];
    }

}
