<?php

if (!function_exists('simpleError')) {

    /**
     * return a MessageBag that can be used for errors
     *
     * @param
     * @return
     */
    function simpleError($error) {
        $errors = new \Illuminate\Support\MessageBag();
        $errors->add('error', $error);
        return $errors;
    }

}
if (!function_exists('inputError')) {

    /**
     * return error infromation for an specific input name form the errors session 
     * 
     * @param String $name
     * @param String $errorClass
     * @return array
     */
    function inputError($name, $errorClass = 'has-error') {
        $name = str_replace(']', '', str_replace('[', '.', $name));
        $errors = session('errors', new \Illuminate\Support\MessageBag);
        
        return $errors->has($name) ? $errors->first($name) : FALSE;
    }

}

if (!function_exists('convertNotifications')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function convertNotifications($notifications) {
        if (!is_array($notifications)) {
            return $notifications;
        }

        $msg = '';
        foreach ($notifications as $notification) {
            $msg = "$notification<br>";
        }

        return $msg;
    }

}
