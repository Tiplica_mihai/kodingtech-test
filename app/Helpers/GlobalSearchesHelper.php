<?php

use App\Models\Management\User;

if (!function_exists('searchUser')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function searchUser($search) {
        return User::where('name', 'like', "%$search%")
                        ->orWhere('email', 'like', "%$search%")
                        ->orWhere('username', 'like', "%$search%")
                        ->select('id')
                        ->pluck('id');
    }

}

