<?php

if (!function_exists('migration_schema_audit')) {

    /**
     * Add specific columns and fk to trace the activity of the users
     *
     * @param
     * @return
     */
    function migration_schema_audit(&$table, $timestamps = true, $softDeletes = true, $columns = ['created_by', 'updated_by', 'deleted_by']) {
        if ($timestamps) {
            $table->timestamps();
        }

        if ($softDeletes) {
            $table->softDeletes();
        }

        foreach ($columns as $column) {
            $table->integer($column)->unsigned()->nullable();
            $table->foreign($column)->references('id')->on('users');
        }
    }

}

if (!function_exists('migration_schema_file')) {

    /**
     * Add specific columns to save a file in the database
     *
     * @param
     * @return
     */
    function migration_schema_file(&$table, $column_prefix) {
        $table->string("{$column_prefix}_file_name")->nullable();
        $table->integer("{$column_prefix}_file_size")->nullable();
        $table->string("{$column_prefix}_content_type")->nullable();
        $table->timestamp("{$column_prefix}_updated_at")->nullable();
        $table->string("{$column_prefix}_variants", 255)->nullable();
    }

}

if (!function_exists('migration_schema_image')) {

    /**
     * Add specific columns to save a image in the database
     *
     * @param
     * @return
     */
    function migration_schema_image(&$table, $column_prefix) {
        $table->string("{$column_prefix}_file_name")->nullable();
        $table->integer("{$column_prefix}_file_size")->nullable();
        $table->string("{$column_prefix}_content_type")->nullable();
        $table->timestamp("{$column_prefix}_updated_at")->nullable();
        $table->string("{$column_prefix}_variants", 255)->nullable();
    }

}
