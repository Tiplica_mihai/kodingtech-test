<?php

if (!function_exists('mime_icon')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function mime_icon($mime) {
        if (strpos($mime, 'spreadsheet')) {
            return "far fa-file-excel";
        } elseif (strpos($mime, 'presentation')) {
            return "far fa-file-powerpoint";
        } elseif (strpos($mime, 'ext/html')) {
            return "far fa-file-word";
        } elseif (strpos($mime, 'pplication/zip')) {
            return "far fa-file-archive";
        } elseif (strpos($mime, 'pplication/rar')) {
            return "far fa-file-archive";
        } elseif (strpos($mime, 'pplication/pdf')) {
            return "far fa-file-pdf";
        } elseif (strpos($mime, 'ext/plain')) {
            return "far fa-file-alt";
        } elseif (strpos($mime, 'udio/')) {
            return "far fa-file-audio";
        } elseif (strpos($mime, 'ideo/')) {
            return "far fa-file-video";
        } elseif (strpos($mime, 'mage/')) {
            return "far fa-file-image";
        } else {
            return "far fa-file";
        }
    }

}

if (!function_exists('mime_types')) {

    /**
     * Get all mime types for specific categories, without the prefix of the 
     * specified mime 
     * 
     * reference: http://www.freeformatter.com/mime-types-list.html
     *   
     * @param type $type
     */
    function mime_types($type) {
        switch ($type) {
            case 'document':
                return [
                    'application/pdf',
                    'application/vnd.amazon.ebook',
                    'application/x-texinfo',
                    'application/vnd.kde.karbon',
                    'application/vnd.kde.kchart',
                    'application/vnd.kde.kformula',
                    'application/vnd.kde.kpresenter',
                    'application/vnd.kde.kspread',
                    'application/vnd.kde.kword',
                    'application/x-latex',
                    'application/vnd.ms-excel',
                    'application/vnd.ms-excel.addin.macroenabled.12',
                    'application/vnd.ms-excel.sheet.binary.macroenabled.12	',
                    'application/vnd.ms-excel.template.macroenabled.12',
                    'application/vnd.ms-excel.sheet.macroenabled.12',
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                    'application/vnd.openxmlformats-officedocument.presentationml.slide',
                    'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
                    'application/vnd.openxmlformats-officedocument.presentationml.template	',
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
                    'application/onenote',
                    'application/vnd.ms-powerpoint',
                    'application/vnd.ms-powerpoint.addin.macroenabled.12',
                    'application/vnd.ms-powerpoint.slide.macroenabled.12',
                    'application/vnd.ms-powerpoint.presentation.macroenabled.12',
                    'application/vnd.ms-powerpoint.slideshow.macroenabled.12	',
                    'application/vnd.ms-powerpoint.template.macroenabled.12',
                    'application/msword',
                    'application/vnd.ms-word.document.macroenabled.12',
                    'application/vnd.ms-word.template.macroenabled.12',
                    'application/vnd.oasis.opendocument.chart',
                    'application/vnd.oasis.opendocument.chart-template',
                    'application/vnd.oasis.opendocument.presentation',
                    'application/vnd.oasis.opendocument.presentation-template',
                    'application/vnd.oasis.opendocument.spreadsheet',
                    'application/vnd.oasis.opendocument.spreadsheet-template',
                    'application/vnd.oasis.opendocument.text',
                    'application/vnd.oasis.opendocument.text-master',
                    'application/vnd.oasis.opendocument.text-template',
                    'application/vnd.sun.xml.calc',
                    'application/vnd.sun.xml.calc.template',
                    'application/vnd.sun.xml.impress',
                    'application/vnd.sun.xml.impress.template',
                    'application/vnd.sun.xml.math',
                    'application/vnd.sun.xml.writer',
                    'application/vnd.sun.xml.writer.global',
                    'application/vnd.sun.xml.writer.template',
                    'application/rtf',
                    'message/rfc822',
                    'text/csv',
                    'text/html',
                    'text/richtext',
                    'text/plain',
                ];
            case 'audio':
                return [
                    'application/x-dtbook+xml',
                ];
            case 'image':
                return [
                    'application/vnd.3gpp.pic-bw-large',
                    'application/vnd.xara',
                    'text/vnd.graphviz',
                ];
            case 'video':
                return [
                    'application/vnd.audiograph',
                    'application/mp4',
                    'application/x-cdlink',
                    '',
                ];
            case 'archive':
                return [
                    'application/x-7z-compressed',
                    'application/vnd.airzip.filesecure.azf',
                    'application/vnd.airzip.filesecure.azs',
                    'application/x-bzip',
                    'application/x-bzip2',
                    'application/x-cpio',
                    'application/x-gtar',
                    'application/x-rar-compressed',
                    'application/x-tar',
                    'application/zip',
                ];
            case 'all':
                return array_merge(
                        mime_types('document'), mime_types('audio'), mime_types('image'), mime_types('video'), mime_types('archive')
                );
            default:
                return [];
        }
    }

}


if (!function_exists('human_filesize')) {

    function human_filesize($bytes, $decimals = 2) {
        $size = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
    }

}
if (!function_exists('rmdir_rf')) {

    function rmdir_rf($dir) {
        foreach (scandir($dir) as $file) {
            if ('.' === $file || '..' === $file)
                continue;
            if (is_dir("$dir/$file"))
                rmdir_recursive("$dir/$file");
            else
                unlink("$dir/$file");
        }
        rmdir($dir);
    }

}