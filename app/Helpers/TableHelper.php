<?php

define('PER_PAGE', env('PER_PAGE', 15));
define('PER_PAGE_SHORT', env('PER_PAGE_SHORT', 5));

define('WEB_PER_PAGE', env('WEB_PER_PAGE', 6));
define('WEB_PER_PAGE_SHORT', env('WEB_PER_PAGE_SHORT', 3));

if (!function_exists('tableRowIndex')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function tableRowIndex($iteration, $perPage = PER_PAGE)
    {
        $offset = request('page') != '' ? request('page') - 1 : 0;
        $offset *= $perPage;
        return $iteration + $offset;
    }
}

if (!function_exists('paginate')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function paginate($model, $align = 'text-right')
    {
        return "<div class='$align'>"
            . $model->appends(request()->except('page'))->render()
            . "</div>";
    }
}

if (!function_exists('attributesModelFilter')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function attributesModelFilter(&$query, $searchAttributes)
    {
        $query->where(function ($q) use ($searchAttributes) {
            foreach ($searchAttributes as $attribute) {
                if (request($attribute)) {
                    strpos($attribute, '_id') ?
                        $q->where($attribute, request($attribute)) : (strpos($attribute, '_at') || strpos($attribute, '_date') ?
                            $q->where($attribute, 'like', carbonFromRequest($attribute)->format('Y-m-d') . "%") : $q->where($attribute, 'like', "%" . request($attribute) . "%"));
                }
            }
        });
    }
}

if (!function_exists('backButton')) {

    /**
     * Generate back button for actions
     */
    function backButton($route)
    {
        return [
            'href' => $route,
            'title' => __('general.btn.back_action'),
            'icon' => 'fas fa-reply',
            'class' => 'btn-default'
        ];
    }
}
