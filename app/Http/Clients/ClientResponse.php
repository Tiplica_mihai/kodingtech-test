<?php

namespace App\Http\Clients;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Mime\Exception\RuntimeException;

/**
 * The purpose of this model is to have all the power
 * of the Eloquent model but to suppress the ORM
 * functionality. All the methods that uses the database
 * will throw a \RuntimeException
 */
class ClientResponse extends Model
{
    private function throwNoActiveRecord(): void
    {
        throw new RuntimeException("This is a ClientResponseModel that has no ActiveRecord functionality");
    }

    public function save(array $options = [])
    {
        $this->throwNoActiveRecord();
    }

    public function saveOrFail(array $options = [])
    {
        $this->throwNoActiveRecord();
    }

    public function update(array $attributes = [], array $options = [])
    {
        $this->throwNoActiveRecord();
    }

    public function delete()
    {
        $this->throwNoActiveRecord();
    }

    public function forceDelete()
    {
        $this->throwNoActiveRecord();
    }

    public function fresh($with = [])
    {
        $this->throwNoActiveRecord();
    }

    public function refresh()
    {
        $this->throwNoActiveRecord();
    }

    public function setConnection($name)
    {
        $this->throwNoActiveRecord();
    }
}
