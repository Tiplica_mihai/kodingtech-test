<?php

namespace App\Http\Clients;

use App\Http\Clients\HttpClient;

class EmagClient extends HttpClient
{
    public function getLaptops($page = 1, $perPage = 10)
    {
        $data = $this->request('GET', "/search-by-url", [
            'query' => [
                'page' => [
                    'limit' => $perPage,
                    'offset' => ($page - 1) * $perPage
                ],
                'url' => $page == 1 ? "/laptopuri/c" : "/laptopuri/p{$page}/c"
            ]
        ]);

        return $data;
    }
}
