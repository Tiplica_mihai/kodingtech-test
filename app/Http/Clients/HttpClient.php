<?php

namespace App\Http\Clients;

use stdClass;
use RuntimeException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Base client.
 *
 * @package App\Http\ServiceClients
 */
class HttpClient
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var string
     */
    protected $clientName;

    /**
     * @var object
     */
    protected $config;

    /**
     * BaseServiceClient constructor.
     *
     * @param stdClass $config
     */
    public function __construct(stdClass $config)
    {
        $this->config = $config;
        $this->clientName = get_class($this);
    }

    /**
     * Implement lazy loading for the Guzzle client.
     */
    private function getClient(): Client
    {
        if ($this->client == null) {
            $this->client = new Client([
                'base_uri' => $this->config->url,
                'timeout' => $this->config->timeout,
                'handler' => $this->config->handlerStack,
            ]);
        }

        return $this->client;
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $options
     * @return array
     */
    public function request($method, $uri, array $options = [])
    {
        try {
            $response = $this->getClient()->request($method, $uri, $this->buildOptions($options));

            $statusCode = $response->getStatusCode();

            $rawResponse = (string) $response->getBody();
            $response = json_decode($rawResponse);

            switch (true) {
                case $statusCode == 422:
                    $this->throwUnprocessableEntity($rawResponse);
                case $statusCode == 404:
                    $this->throwNotFoundException($uri);
                case intval($statusCode / 100) != 2:
                    $content = $rawResponse ?? $response->error->message;

                    throw new RuntimeException(
                        "{$this->clientName} request failed {$statusCode}: "
                            . ($content ?? 'No error provided')
                    );
            }

            return $response;
        } catch (RequestException $e) {
            throw new RuntimeException("{$this->clientName} request failed: {$e->getMessage()}", 0, $e);
        }
    }

    /**
     * Validate the data before sending the request. In case
     * the validator fails, it will throw validation exception
     *
     * @param array $data
     * @param array $rules
     * @return void
     * @throws ValidationException
     */
    public function validateData(array $data = [], array $rules = []): void
    {
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            throw (new ValidationException($validator));
        }
    }

    private function buildOptions($options)
    {
        $baseOptions = [
            // 'decode_content' => 'gzip',
            'http_errors' => false,
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.100 Safari/537.36',
                //'Accept'     => 'application/json',
            ]
        ];

        if (isset($this->config->key)) {
            $baseOptions['headers']['Authorization'] = $this->config->key;
        }

        return array_merge_recursive($baseOptions, $options);
    }


    /**
     * Method to declare how to throw a 422 error
     */
    protected function throwUnprocessableEntity($rawResponse)
    {
        throw new UnprocessableEntityHttpException($rawResponse);
    }

    /**
     * Method to declare how to throw a 404 error
     */
    protected function throwNotFoundException($uri)
    {
        throw new NotFoundHttpException("Resource not found at {$uri}");
    }

}
