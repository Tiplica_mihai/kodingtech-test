<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Users\SearchUsersRequest as SearchRequest;
use App\Http\Controllers\Controller;


class UsersController extends Controller
{

    /**
     * Display a listing of the resource.
     */
    public function index(SearchRequest $request)
    {
        $results = $request->getData();

        return response()->json([
            "status" => count($results) != 0 ? 200 : false, "error" => NULL,
            "data" => ["results" => $results]
        ])->withHeaders([
            "Cache-Control" => "no-cache, no-store, must-revalidate",
            "Pragma" => "no-cache", "Expires" => "0"
        ]);
    }
}
