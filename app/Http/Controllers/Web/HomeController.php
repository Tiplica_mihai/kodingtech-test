<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::when($request->name, function ($q) use ($request) {
            $q->where('name', 'like', "%{$request->name}%");
        })->when($request->part_number, function ($q) use ($request) {
            $q->where('part_number', 'like', "%{$request->part_number}%");
        })->when($request->min_price, function ($q) use ($request) {
            $q->where('price', '>=', $request->min_price);
        })->when($request->max_price, function ($q) use ($request) {
            $q->where('price', '<=', $request->max_price);
        })->paginate(60);

        return view('web.home.index')
            ->with('products', $products);
    }
}
