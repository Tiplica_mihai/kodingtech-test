<?php

namespace App\Http\Requests\Api\Users;

use Illuminate\Foundation\Http\FormRequest;
use \App\Traits\Requests\IndexRequestTrait;
use App\Models\Management\User;

class SearchUsersRequest extends FormRequest
{

    use IndexRequestTrait;

    public function getData()
    {
        $search = $this->search;

        return User::Where('name', 'like', "%$search%")
            ->orWhere('email', 'like', "%$search%")
            ->limit(10)->get()->map(function ($user) {
                return [
                    'id' => $user->id,
                    'email' => $user->email,
                    'name' => "$user->name ({$user->email})",
                ];
            });
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'search' => 'required|max:64',
        ];
    }
}
