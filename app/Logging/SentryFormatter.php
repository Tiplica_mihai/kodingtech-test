<?php

namespace App\Logging;

use Monolog\Formatter\LineFormatter;

/**
 * Custom sentry log error formatter
 */
class SentryFormatter
{
    /**
     * Customize the given logger instance.
     *
     * @param  \Illuminate\Log\Logger  $logger
     * @return void
     */
    public function __invoke($logger)
    {
        foreach ($logger->getHandlers() as $handler) {
            $formatter = new LineFormatter("%channel%.%level_name%: %message%\n", 'Y-m-d H:i:s', true, true);
            $formatter->includeStacktraces();

            $handler->setFormatter($formatter);
        }
    }
}
