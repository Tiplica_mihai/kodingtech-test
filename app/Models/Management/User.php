<?php

namespace App\Models\Management;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Czim\Paperclip\Contracts\AttachableInterface;
use Czim\Paperclip\Model\PaperclipTrait;
use App\Traits\UserActions;

class User extends Authenticatable implements AttachableInterface
{
    use Notifiable,
        SoftDeletes,
        UserActions,
        PaperclipTrait;


    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table name of the model
     *
     * @var String
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'avatar'
    ];

    /**
     * The input fields that should be taken from request forms at store and update.
     */
    const FORM_DATA = [
        'name',
        'email',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'email_verified_at',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function __construct(array $attributes = [])
    {
        $this->hasAttachedFile('avatar', [
            'variants' => [
                'medium' => '300x300',
                'thumb' => '100x100',
            ],
            'attributes' => [
                'variants' => true,
            ],
        ]);

        parent::__construct($attributes);
    }

    public static function selectable()
    {
        return self::orderBy('name')->get()->mapWithKeys(function ($item) {
            return [$item->id => "$item->name ($item->email)"];
        });
    }

    /**
     * Function to hash the password when creating a new user
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make($value);
    }

    public function getAvatarImgAttribute()
    {
        return $this->avatar_file_name
            ? $this->avatar->url('thumb')
            : url('private/img/user_no_avatar.png');
    }
}
