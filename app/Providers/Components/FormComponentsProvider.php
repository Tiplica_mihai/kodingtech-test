<?php

namespace App\Providers\Components;

use Illuminate\Support\ServiceProvider;

class FormComponentsProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        \Form::macro('appOpenFormGroup', function ($label, $name, $width, $properties = []) {
            $required = isset($properties['required']) || in_array('required', $properties) ? '<span class="required text-danger">*</span>' : '';
            $hasError = inputError($name) ? 'has-error' : '';
            $info = null;


            if (isset($properties['data-info-tooltip'])) {
                $info = view('components.form.info_tooltip')->with([
                    'info' => $properties['data-info-tooltip']
                ])->render();

                unset($properties['data-info-tooltip']);
            }

            return "<div class='control-label col-md-$width col-sm-12 col-xs-12 less-padding'>"
                . "<div class='form-group $hasError'>"
                . "<label>$label $required </label> $info";
        });

        \Form::macro('appCloseFormGroup', function ($name, $customHtml = null, $withInputMessage = true) {
            $message = ($name ? '&nbsp;' : '') . inputError($name);

            return "$customHtml"
                . ($withInputMessage ? "<span class='help-block input-message'>"
                . "<strong>$message</strong>"
                . "</span>" : '')
                . "</div>"
                . "</div>";
        });

        \Form::macro('legend', function ($title) {
            return view('components.form.legend')->with('title', $title)->render();
        });

        \Form::macro('requiredNotificationAndSubmit', function ($cancelRoute, $ok = '<i class="far fa-save"></i> Save', $cancel = '<i class="fas fa-times"></i> Cancel') {
            return view('components.form.required_notification')
                ->with('cancelRoute', $cancelRoute)
                ->with('okLabel', $ok)
                ->with('cancelLabel', $cancel)
                ->render();
        });

        \Form::macro('appLabel', function ($label, $value, $width = 12, $nullValue = '', $class = '') {
            $value = $value ? $value : $nullValue;
            return \Form::appOpenFormGroup($label, null, $width)
                . "<div class='$class'>$value</div>"
                . \Form::appCloseFormGroup(null, null, false);
        });

        \Form::macro('appText', function ($label, $name, $value = null, $width = 12, $properties = []) {
            $properties = array_merge(['class' => 'form-control input-sm', 'autocomplete' => 'off'], $properties);

            return \Form::appOpenFormGroup($label, $name, $width, $properties)
                . \Form::text($name, $value, $properties)
                . \Form::appCloseFormGroup($name);
        });

        \Form::macro('appNumber', function ($label, $name, $value = null, $width = 12, $properties = []) {
            $properties = array_merge(['class' => 'form-control input-sm', 'autocomplete' => 'off'], $properties);

            return \Form::appOpenFormGroup($label, $name, $width, $properties)
                . \Form::number($name, $value, $properties)
                . \Form::appCloseFormGroup($name);
        });

        \Form::macro('hiddenArray', function ($name, $values) {
            $data = "";
            $name .= '[]';
            foreach ($values as $v) {
                $data .= "<input type = 'hidden' name = '$name' value = '$v' />";
            }

            return $data;
        });

        \Form::macro('appTextarea', function ($label, $name, $value = null, $width = 12, $properties = []) {
            $properties = array_merge(['class' => 'form-control input-sm', 'autocomplete' => 'off'], $properties);


            return \Form::appOpenFormGroup($label, $name, $width, $properties)
                . \Form::textarea($name, $value, $properties)
                . \Form::appCloseFormGroup($name);
        });

        \Form::macro('appEmail', function ($label, $name, $value = null, $width = 12, $properties = []) {
            $properties = array_merge(['class' => 'form-control input-sm', 'autocomplete' => 'off'], $properties);


            return \Form::appOpenFormGroup($label, $name, $width, $properties)
                . \Form::email($name, $value, $properties)
                . \Form::appCloseFormGroup($name);
        });

        \Form::macro('appPassword', function ($label, $name, $width = 12, $properties = []) {
            $properties = array_merge(['class' => 'form-control input-sm', 'autocomplete' => 'off'], $properties);

            return \Form::appOpenFormGroup($label, $name, $width, $properties)
                . \Form::password($name, $properties)
                . \Form::appCloseFormGroup($name);
        });

        \Form::macro('appSelect', function ($label, $name, $values, $value, $width = 12, $properties = []) {
            $properties = array_merge(['class' => 'form-control input-sm chosen', 'placeholder' => $label], $properties);

            return \Form::appOpenFormGroup($label, $name, $width, $properties)
                . \Form::select($name, $values, $value, $properties)
                . \Form::appCloseFormGroup($name);
        });

        \Form::macro('appSubmit', function ($cancelRoute = '#', $ok = '<i class="far fa-save"></i> Salveaza', $cancel = '<i class="fas fa-times"></i> Anuleaza') {
            return "<div class = 'pull-left'>"
                . "<button type = 'submit' class = 'btn btn-flat btn-success'>$ok</button> "
                . "<a class='btn btn-flat btn-primary' href='$cancelRoute'>$cancel</a>"
                . "</div>";
        });


        \Form::macro('appSummernote', function ($label, $name, $value = null, $width = 12, $properties = []) {
            $properties = array_merge(['class' => 'form-control input-sm', 'autocomplete' => 'off'], $properties);
            $old = $value != null ? $value : old($name);

            return view('components.form.summernote')->with([
                'label' => $label, 'name' => $name,
                'value' => $value, 'old' => $old,
                'width' => $width, 'properties' => $properties
            ])->render();
        });

        \Form::macro('appFile', function ($label, $name, $value = null, $width = 12, $properties = [], $customMessage = null) {
            $properties = array_merge(['class' => 'form-control input-sm', 'autocomplete' => 'off'], $properties);

            return \Form::appOpenFormGroup($label, $name, $width, $properties)
                . \Form::file($name, $properties)
                . \Form::appCloseFormGroup($name, "<small>$customMessage</small>");
        });


        \Form::macro('appToggle', function ($label, $name, $value = null, $width = 12, $properties = []) {
            $properties = array_merge(['data-toggle' => 'toggle', 'data-size' => 'small', 'data-on' => 'Yes', 'data-off' => 'No'], $properties);

            return \Form::appOpenFormGroup($label, $name, $width, $properties)
                . (isset($properties['data-inline']) && $properties['data-inline'] ? '&nbsp;&nbsp;' : '<br>')
                . \Form::checkbox($name, '1', (bool)$value, $properties)
                . \Form::appCloseFormGroup($name);
        });

        \Form::macro('appCheck', function ($label, $name, $value = null, $width = 12, $properties = []) {
            $properties = array_merge(['id' => $name], $properties);

            return \Form::appOpenFormGroup($label, $name, $width, $properties)
                . (isset($properties['data-inline']) && $properties['data-inline'] ? '&nbsp;&nbsp;' : '<br>')
                . \Form::checkbox($name, '1', (bool)$value, $properties)
                . \Form::appCloseFormGroup($name);
        });

        \Form::macro('appInlineCheck', function ($label, $name, $value = null, $properties = []) {
            $input = \Form::checkbox($name, '1', (bool)$value, $properties);
            $label = "<label>{$label}</label>";

            return $input . ' ' . $label;
        });


        \Form::macro('appDate', function ($label, $name, $value = null, $width = 12, $properties = [], $jsProperites = null) {
            $id = str_replace('[', '', str_replace(']', '', $name)) . '-datetimepicker';
            $properties = array_merge(['id' => $id, 'class' => 'form-control input-sm', 'autocomplete' => 'off'], $properties);

            return \Form::appOpenFormGroup($label, $name, $width, $properties)
                . \Form::text($name, $value, $properties)
                . \Form::appCloseFormGroup($name)
                . "<script>"
                . "$(document).ready(function(){ "
                . "$('#$id').datetimepicker({ format: 'DD-MM-YYYY', $jsProperites}); });"
                . "</script>";
        });

        \Form::macro('appDateTime', function ($label, $name, $value = null, $width = 12, $properties = [], $jsProperites = null) {
            $id = str_replace('[', '', str_replace(']', '', $name)) . '-datetimepicker';
            $properties = array_merge(['id' => $id, 'class' => 'form-control input-sm', 'autocomplete' => 'off'], $properties);

            return \Form::appOpenFormGroup($label, $name, $width, $properties)
                . \Form::text($name, $value, $properties)
                . \Form::appCloseFormGroup($name)
                . "<script>"
                . "$(document).ready(function(){ "
                . "$('#$id').datetimepicker({ sideBySide: true, format: '" . DATE_TIME_PICKER_FORMAT_JS . "', $jsProperites}); });"
                . "</script>";
        });


        \Form::macro('appTypeahead', function ($label, $name, $value, $url, $width = 12, $required = false, $extraData = false, $callbackFunction = null, $cancelFunction = null, $properties = []) {
            return view('components.form.typeahead')->with([
                'label' => $label, 'name' => $name,
                'value' => $value ? (object)$value : (object)['id' => null, 'label' => null],
                'url' => $url, 'width' => $width, 'extraData' => $extraData,
                'required' => $required, 'inputError' => inputError($name),
                'properties' => array_merge($properties, [
                    'placeholder' => $label,
                    'class' => 'form-control input-sm',
                    'type' => 'search',
                    'autocomplete' => 'off',
                    'id' => $name
                ]),
                'callbackFunction' => $callbackFunction, 'cancelFunction' => $cancelFunction,
            ])->render();
        });

        \Form::macro('appPoolOption', function ($name, $callback, $data, $required = false) {
            $requiredAttr = $required ? "required=''" : '';
            return "<div class='input-group m-b-sm'>"
                . "<input value='$data' type='text' class='form-control' name='$name' $requiredAttr placeholder='Optiune'>"
                . "<span class='input-group-btn'>"
                . "<span type='button' class='btn btn-danger' onclick='$callback'><i class='fa fa-times'></i></span> "
                . "</span>"
                . "</div>";
        });

        \Form::macro('appPoolOptions', function ($name, $label, $required = false, $size = null) {
            $errorClass = inputError($name);
            $requiredSpan = ($required ? ' <span class="text-danger">*</span>' : '');
            $size = $size == null ? 'col-md-8 col-lg-5' : $size;
            $newOption = "<span class='btn btn-xs btn-primary btn-flat m-t-sm m-b-sm' id='new$name'><i class='fas fa-plus'></i> Adauga Optiune</span>";

            $newInput = \Form::appPoolOption($name . '[]', 'removeOption' . $name . '($(this))', null, $required);

            $input = $newInput;
            if (old($name)) {
                foreach (old($name) as $option) {
                    $input .= \Html::appPoolOption($name . '[]', 'removeOption' . $name . '($(this))', $option, $required);
                }
            }

            return " <div class='form-group $errorClass'>
                        <label class='col-sm-2 control-label'> $label $requiredSpan <br>$newOption</label>
                        <div class='$size' id='$name'>$input</div>
                    </div>
                    <script>
                    function removeOption$name(obj) {
                        obj.parent().parent().remove();
                    }
                    $(document).ready(function () {
                        $('#new$name').click(function () {
                            $('#$name').append(\"$newInput\");
                        });
                    });
                    </script>";
        });



        \Form::macro('divider', function () {
            return "<div class='ln_solid'></div>";
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
//
    }

}
