<?php

namespace App\Providers\Components;

use Illuminate\Support\ServiceProvider;

class GlobalComponentsProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        \Html::macro('openPanel', function($title, $class = 'box-primary') {
            return view('components.global.panels.open')->with([
                        'title' => $title,
                        'class' => $class,
                    ])->render();
        });

        \Html::macro('closePanel', function() {
            return view('components.global.panels.close')
                            ->render();
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}
