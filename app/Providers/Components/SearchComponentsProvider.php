<?php

namespace App\Providers\Components;

use Illuminate\Support\ServiceProvider;

class SearchComponentsProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        \Form::macro('openSearchPanel', function($customForm = false, $resetUrl = null) {
            $resetUrl = $resetUrl ? $resetUrl : request()->url();
            return "<div class='box box-success'>"
                    . \Form::open($customForm ? $customForm : ['method' => 'GET'])
                    . "<div class='box-header'>"
                    . " <i class='fa fa-filter'></i>"
                    . "<h3 class='box-title'>Search</h3>"
                    . "<div class='box-tools pull-right'>"
                    . "<button type='submit' class='btn btn-primary btn-flat btn-xs m-r-xs' data-toggle='tooltip' title='' data-original-title='Search'><i class='fa fa-search'></i></button>"
                    . "<a type='reset' href='$resetUrl' class='btn btn-warning btn-flat btn-xs' data-toggle='tooltip' title='' data-original-title='Clear'><i class='fa fa-eraser'></i></a>"
                    . "</div>"
                    . "</div>"
                    . "<div>";
        });

        \Form::macro('closeSearchPanel', function() {
            return "<div class='clearfix'></div></div>"
                    . \Form::close()
                    . "</div>";
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}
