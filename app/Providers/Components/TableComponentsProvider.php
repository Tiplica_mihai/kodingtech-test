<?php

namespace App\Providers\Components;

use Illuminate\Support\ServiceProvider;

class TableComponentsProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        \Html::macro('editButton', function($route, $id) {
            return "<a class='btn btn-flat btn-warning btn-xs btn-action' title='Editeaza' href='" . route($route, $id) . "'><i class='fa fa-edit'></i></a>";
        });

        \Html::macro('customButton', function($url, $title, $icon, $class = 'btn-primary', $label = NULL, $target = false) {
            return "<a class='btn btn-flat $class btn-xs " . ($label ? '' : 'btn-action' ) . "' title='$title' href='$url' " . ($target ? "target='$target'" : '') . "><i class='fa $icon'></i> $label</a>";
        });

        \Html::macro('customActionButton', function($action, $title, $icon, $class = 'btn-primary', $encloseForm = FALSE) {
            $action = str_replace("'", "\\'", $action);
            $btn = $encloseForm ? \Form::appOpenFormGroup(null, null) : '';
            $btn .= "<a class='btn btn-flat btn-outline $class btn-xs btn-action' title='$title' onclick='$action'><i class='fa $icon'></i> $title</a>";
            $btn .= $encloseForm ? \Form::appCloseFormGroup(null) : '';
            return $btn;
        });

        \Html::macro('viewButton', function($route, $id) {
            return "<a class='btn btn-flat btn-primary btn-xs btn-action' title='Vizualizeaza' href='" . route($route, $id) . "'><i class='far fa-eye'></i></a>";
        });


        \Html::macro('deleteButton', function($route, $id, $isActive = false, $title = "Schimbare stare", $message = "Are you sure you want to change the state of the resource?") {
            $realId = is_array($id) ? $id[count($id) - 1] : $id;
            $state = !$isActive ? 'btn-danger' : 'btn-success';
            $icon = !$isActive ? 'fa-times' : 'fa-check';
            $title = $isActive ? 'Activate' : 'Delete';

            return view('components.buttons.delete')->with([
                        'route' => $route, 'id' => $id,
                        'realId' => $realId, 'formId' => 'delete-form',
                        'state' => $state, 'icon' => $icon,
                        'title' => $title, 'message' => $message,
                    ])->render();
        });

        \Html::macro('deleteButtonMicro', function($route, $id, $isActive = false, $title = "Schimbare stare", $message = "Are you sure you want to change the state of the resource?") {
            $realId = is_array($id) ? $id[count($id) - 1] : $id;
            $state = !$isActive ? 'btn-danger' : 'btn-success';
            $icon = !$isActive ? 'fa-times' : 'fa-check';
            $title = $isActive ? 'Activate' : 'Delete';

            return view('components.buttons.delete_micro')->with([
                        'route' => $route, 'id' => $id,
                        'realId' => $realId, 'formId' => 'delete-form',
                        'state' => $state, 'icon' => $icon,
                        'title' => $title, 'message' => $message,
                    ])->render();
        });

        \Html::macro('deleteButtonWithReason', function($route, $id, $title, $message = "Are you sure you want to delete the resource?") {
            $realId = is_array($id) ? $id[count($id) - 1] : $id;
            $title = $title ? $title : 'Delete';
            $formId = 'delete-form-with-reason';

            return "<a class='btn btn-flat btn-xs btn-danger btn-action' title='Delete' href='" . route($route, $id) . "' "
                    . "  onclick=\"event.preventDefault(); deleteWithReasonResource('$title', '$message', '$formId-$realId')\" >"
                    . "<i class='fa fa-times'></i></a>"
                    . \Form::open(['url' => route($route, $id), 'method' => 'DELETE', 'id' => "$formId-$realId", 'class' => 'hidden'])
                    . \Form::hidden('delete_reason')
                    . \Form::close();
        });

        \Html::macro('openTable', function($headers, $actions = null, $hasNrCrt = true, $alignLastRight = true, $title = 'List Results', $hasSearch = true) {
            return view('components.table.open_table')->with([
                        'tableTitle' => $title, 'headers' => $headers,
                        'actions' => $actions, 'hasNrCrt' => $hasNrCrt,
                        'alignLastRight' => $alignLastRight, 'orderedContent' => false,
                        'hasSearch' => $hasSearch
                    ])->render();
        });

        \Html::macro('openSimpleTable', function($headers, $hasNrCrt = true, $alignLastRight = false) {
            return view('components.table.open_simple_table')->with([
                        'headers' => $headers,
                        'hasNrCrt' => $hasNrCrt,
                        'alignLastRight' => $alignLastRight,
                        'orderedContent' => [],
                    ])->render();
        });

        \Html::macro('closeTable', function($model = NULL, $path = false) {
            return view('components.table.close_table')->with([
                        'model' => $model, 'path' => $path,
                    ])->render();
        });

        \Html::macro('closeSimpleTable', function() {
            return view('components.table.close_simple_table')->render();
        });

        \Html::macro('tr_no_results', function($headers) {
            return view('components.table.no_results')->with([
                        'tableHeaders' => $headers,
                    ])->render();
        });

        \Html::macro('td_nr', function($loop, $per_page = PER_PAGE) {
            return "<th scope='row' class='text-right'>" . tableRowIndex($loop->iteration, $per_page) . "</th>";
        });

        \Html::macro('td', function($value, $tooltipLength = 20, $nullValue = 'N/A', $tooltipPlacement = 'top', $class = 'text-left') {
            $value = $value ? $value : $nullValue;
            $tooltip = $tooltipLength > 0 ? "data-toggle='tooltip' data-placement='$tooltipPlacement' data-container='body' data-html='true' title='$value'" : "";
            return "<td class='$class' $tooltip>"
                    . ($tooltipLength > 0 ? str_limit($value, $tooltipLength) : $value)
                    . "</td>";
        });

        \Html::macro('td_a', function($url, $value, $tooltipLength = 20, $nullValue = 'N/A', $tooltipPlacement = 'top') {
            $value = $value ? $value : $nullValue;
            $tooltip = $tooltipLength > 0 ? "data-toggle='tooltip' data-placement='$tooltipPlacement' data-container='body' data-html='true' title='$value'" : "";
            return "<td class='text-left' $tooltip>"
                    . "<a href='$url'>"
                    . ($tooltipLength > 0 ? str_limit($value, $tooltipLength) : $value)
                    . "</a>"
                    . "</td>";
        });

        \Html::macro('simpleSearch', function( $url, $name, $placeholder, $method = 'GET', $btnName = '<i class="fa fa-search"></i> Search') {
            return view('components.table.simple_search')->with([
                        'url' => $url, 'name' => $name, 'placeholder' => $placeholder,
                        'method' => $method, 'btnName' => $btnName,
                    ])->render();
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}
