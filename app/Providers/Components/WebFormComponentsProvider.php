<?php

namespace App\Providers\Components;

use Illuminate\Support\ServiceProvider;

class WebFormComponentsProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        \Form::macro('webOpenFormGroup', function ($width) {
            return "<div class='col-md-{$width}'>"
                . "<div class='form-group'>";
        });

        \Form::macro('webCloseFormGroup', function ($name) {
            $errMessage = inputError($name);
            $errMessage = $errMessage ? "<div class='invalid-feedback'>{$errMessage}</div>" : '';

            return "$errMessage"
                . "</div>"
                . "</div>";
        });


        \Form::macro('webText', function ($label, $name, $value = null, $width = 12, $properties = []) {
            $required = isset($properties['required']) || in_array('required', $properties) ? '*' : '';
            $hasError = inputError($name) ? 'is-invalid' : '';

            $properties = array_merge(['class' => "form-control {$hasError}", 'placeholder' => "{$label} {$required}"], $properties);

            return \Form::webOpenFormGroup($width)
                . \Form::text($name, $value, $properties)
                . \Form::webCloseFormGroup($name);
        });

        \Form::macro('webNumber', function ($label, $name, $value = null, $width = 12, $properties = []) {
            $required = isset($properties['required']) || in_array('required', $properties) ? '*' : '';
            $hasError = inputError($name) ? 'is-invalid' : '';

            $properties = array_merge(['class' => "form-control {$hasError}", 'placeholder' => "{$label} {$required}"], $properties);

            return \Form::webOpenFormGroup($width)
                . \Form::number($name, $value, $properties)
                . \Form::webCloseFormGroup($name);
        });

        \Form::macro('webTextarea', function ($label, $name, $value = null, $width = 12, $properties = []) {
            $required = isset($properties['required']) || in_array('required', $properties) ? '*' : '';
            $hasError = inputError($name) ? 'is-invalid' : '';

            $properties = array_merge(['class' => "form-control {$hasError}", 'placeholder' => "{$label} {$required}"], $properties);

            return \Form::webOpenFormGroup($width)
                . \Form::textarea($name, $value, $properties)
                . \Form::webCloseFormGroup($name);
        });

        \Form::macro('webEmail', function ($label, $name, $value = null, $width = 12, $properties = []) {
            $required = isset($properties['required']) || in_array('required', $properties) ? '*' : '';
            $hasError = inputError($name) ? 'is-invalid' : '';

            $properties = array_merge(['class' => "form-control {$hasError}", 'placeholder' => "{$label} {$required}"], $properties);

            return \Form::webOpenFormGroup($width)
                . \Form::email($name, $value, $properties)
                . \Form::webCloseFormGroup($name);
        });

        \Form::macro('webFile', function ($label, $name, $width = 12, $properties = []) {
            $required = isset($properties['required']) || in_array('required', $properties) ? '*' : '';
            $hasError = inputError($name) ? 'is-invalid' : '';

            $properties = array_merge(['class' => "custom-file-input {$hasError}", 'id' => $name], $properties);

            return \Form::webOpenFormGroup($width)
                . "<div class='custom-file'>"
                . \Form::file($name, $properties)
                . "<label for='{$name}' class='custom-file-label primary-color' style='font-size:15px'>{$label} {$required}</label>"
                . "</div>"
                . \Form::webCloseFormGroup($name)
                . "<script>
                $('#{$name}').on('change', function() {
                  $(this).siblings('.custom-file-label').addClass('selected').html( $(this).val().split('\\\\').pop() );
                });</script>";
        });

        \Form::macro('webCheckbox', function ($label, $name, $isChecked, $width = 12, $properties = []) {
            $properties = array_merge(['class' => "form-check-input", 'id' => $name], $properties);

            return "<div class='col-md-{$width}'>"
                . "<div class='container ml-2'>"
                . "<div class='checkbox checkbox-theme form-group'>"
                . \Form::checkbox($name, 'true', $isChecked, $properties)
                . "<label class='form-check-label' for='{$name}'>{$label}</label>"
                . "</div>"
                . "</div>"
                . "</div>";
        });

        \Form::macro('webSubmit', function ($label, $message = "Toate câmpurile sunt obligatorii.") {

            return "<div class='col-md-12'>"
                . "<div class='float-right text-muted small'>{$message}</div>"
                . "<div class='send-btn mt-5'>"
                . "<button type='submit' class='btn btn-md button-theme'>{$label}</button>"
                . "</div>"
                . "</div>";
        });

        /**
         * Inputs used for search
         */

        \Form::macro('webOpenFormGroupWithLabel', function ($width, $label) {
            return "<div class='col-md-{$width} col-sm-6 col-xs-12 px-1'>"
                . "<div class='form-group'>"
                . "<label>{$label}</label>";
        });

        \Form::macro('webCloseFormGroupWithLabel', function ($name) {
            $errMessage = inputError($name);
            $errMessage = $errMessage ? "<div class='invalid-feedback'>{$errMessage}</div>" : '';

            return "$errMessage"
                . "</div>"
                . "</div>";
        });

        \Form::macro('webTextWithLabel', function ($label, $name, $value = null, $width = 12, $properties = []) {
            $required = isset($properties['required']) || in_array('required', $properties) ? '*' : '';
            $hasError = inputError($name) ? 'is-invalid' : '';

            $properties = array_merge(['class' => "form-control {$hasError}", 'placeholder' => "{$label} {$required}"], $properties);

            return \Form::webOpenFormGroupWithLabel($width, $label)
                . \Form::text($name, $value, $properties)
                . \Form::webCloseFormGroupWithLabel($name);
        });

        \Form::macro('webNumberWithLabel', function ($label, $name, $value = null, $width = 12, $properties = []) {
            $required = isset($properties['required']) || in_array('required', $properties) ? '*' : '';
            $hasError = inputError($name) ? 'is-invalid' : '';

            $properties = array_merge(['class' => "form-control {$hasError}", 'placeholder' => "{$label} {$required}"], $properties);

            return \Form::webOpenFormGroupWithLabel($width, $label)
                . \Form::number($name, $value, $properties)
                . \Form::webCloseFormGroupWithLabel($name);
        });

        \Form::macro('webSelectWithLabel', function ($label, $name, $values = [], $value = null, $width = 12, $properties = []) {
            $required = isset($properties['required']) || in_array('required', $properties) ? '*' : '';
            $hasError = inputError($name) ? 'is-invalid' : '';

            $properties = array_merge(['class' => "form-control {$hasError}", 'placeholder' => "{$label} {$required}"], $properties);

            return \Form::webOpenFormGroupWithLabel($width, $label)
                . \Form::select($name, $values, $value, $properties)
                . \Form::webCloseFormGroupWithLabel($name);
        });

        \Form::macro('webTextareaWithLabel', function ($label, $name, $value = null, $width = 12, $properties = []) {
            $required = isset($properties['required']) || in_array('required', $properties) ? '*' : '';
            $hasError = inputError($name) ? 'is-invalid' : '';

            $properties = array_merge(['class' => "form-control {$hasError}", 'placeholder' => "{$label} {$required}"], $properties);

            return \Form::webOpenFormGroupWithLabel($width, $label)
                . \Form::textarea($name, $value, $properties)
                . \Form::webCloseFormGroupWithLabel($name);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
