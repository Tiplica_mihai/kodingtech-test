<?php

namespace App\Providers;

use App\Http\Clients\EmagClient;
use GuzzleHttp\Middleware;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use Illuminate\Support\ServiceProvider;

class HttpClientsProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerServiceClient(EmagClient::class, 'emag');
    }
    /**
     * Register a microservice singleton in the application.
     *
     * @param string $class
     * @param string $clientIdentifier
     */
    private function registerServiceClient(string $class, string $clientIdentifier): void
    {
        $this->app->singleton($class, function ($app) use ($class, $clientIdentifier) {
            $config = $this->getConfig($clientIdentifier);
            $config->handlerStack = $this->getServiceClientLogger();

            return new $class($config);
        });
    }

    /**
     * Get configuration properties for microservice
     * In the case there is missing a configuration
     * for the specified microservice an \RuntimeException
     * will be thrown
     *
     * @param string $identifier
     */
    protected function getConfig($identifier)
    {
        $config = (object) config("http-clients.{$identifier}");

        if (!$config->url || !$config->timeout) {
            throw new \RuntimeException("Missing configurations for '$identifier' http client!
                - Found:" . json_encode($config));
        }

        return $config;
    }

    /**
     * Register a logger for the microservice and returns
     * a Handler thah can be used in Guzzle to log requests.
     *
     * @return GuzzleHttp\HandlerStack
     */
    protected function getServiceClientLogger(): HandlerStack
    {
        $logger = \Log::channel('http-clients');

        $handler = HandlerStack::create();
        $formatter = new MessageFormatter(MessageFormatter::DEBUG);
        $handler->push(Middleware::log($logger, $formatter), 'log_request_response');

        return $handler;
    }
}
