<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class RouteModelProvider extends ServiceProvider
{

    protected $models = [
        // 'district_web' => \App\Models\Locations\District::class,
    ];

    protected $softdeleteModels = [
        'user' => \App\Models\Management\User::class,
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        /**
         * Regex match for route parameters
         */
        collect($this->models)
            ->merge($this->softdeleteModels)->keys()
            ->each(function ($parameter) use ($router) {
                Route::pattern($parameter, '[0-9]+');
            });

        /**
         * Bind models that will resolve also soft deleted models
         * in the controller without throwing a 404 exception
         */
        foreach ($this->softdeleteModels as $key => $model) {
            $router->bind($key, function ($value) use ($model) {
                return ($model)::withTrashed()->findOrFail($value);
            });
        }

        foreach ($this->models as $key => $model) {
            $router->model($key, $model);
        }

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
