<?php

namespace App\Traits;

use App\Models\Management\User;

trait UserActions {

    public function creator() {
        return $this->hasOne(User::class, 'id', 'created_by')->withTrashed();
    }

    public function updater() {
        return $this->hasOne(User::class, 'id', 'updated_by')->withTrashed();
    }

    public function destroyer() {
        return $this->hasOne(User::class, 'id', 'deleted_by')->withTrashed();
    }

    public function toggleModel() {
        $this->trashed() ? $this->update(['deleted_by' => null]) : $this->update(['deleted_by' => \Auth::user()->id]);
        $this->trashed() ? $this->restore() : $this->delete();
    }

    public function getCreatorNameAttribute() {
        return $this->creator ? $this->creator->name : 'N/A';
    }

    public function getUpdaterNameAttribute() {
        return $this->updater ? $this->updater->name : 'N/A';
    }

    public function getDestroyerNameAttribute() {
        return $this->destroyer ? $this->destroyer->name : 'N/A';
    }

    public static function createWithUser($attributes) {
        if (!isset($attributes ['created_by'])) {
            $attributes = array_merge($attributes, ['created_by' => \Auth::user()->id]);
        }

        return parent::create($attributes);
    }

    public function updateWithUser($attributes) {
        if (!isset($attributes['updated_by'])) {
            $attributes = array_merge($attributes, ['updated_by' => \Auth::user()->id]);
        }

        $this->update($attributes);
    }

}
