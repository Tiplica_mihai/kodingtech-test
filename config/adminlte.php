<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => '',

    'title_prefix' => env('APP_NAME', 'Green Alley') . ' - ',

    'title_postfix' => ' Admin',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<b>Green</b>Alley',

    'logo_mini' => '<b>G</b>A',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | light variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'black',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => null,

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'admin',

    'logout_url' => 'admin/logout',

    'logout_method' => null,

    'login_url' => 'admin/login',

    'register_url' => null,

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and a URL. You can also specify an icon from Font
    | Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    */

    'menu' => [
        // [
        //     'text' => 'search',
        //     'search' => true,
        // ],
        ['header' => 'main_navigation'],
        [
            'text' => 'Dashboard',
            'url'  => 'admin/dashboard',
            'icon' => 'fas fa-fw fa-tachometer-alt',
        ],


        ['header' => 'BLOG'],
        [
            'text' => 'Blog Pages',
            'url'  => 'admin/blog',
            'icon' => 'fas fa-fw fa-blog',
            'active' => [
                'regex:@^admin/blog(/[0-9]+/edit)?(/create)?$@'
            ],
        ],
        [
            'text' => 'Blog Categories',
            'url'  => 'admin/blog_categories',
            'icon' => 'fas fa-fw fa-box-open',
            'active' => [
                'regex:@^admin/blog_categories(/[0-9]+/edit)?(/create)?$@'
            ],
        ],

        ['header' => 'MEDIA'],
        [
            'text' => 'Resources',
            'url'  => 'admin/resources',
            'icon' => 'fas fa-fw fa-parachute-box',
            'active' => [
                'regex:@^admin/resources(/[0-9]+/edit)?(/create)?$@'
            ],
        ],
        [
            'text' => 'Press',
            'url'  => 'admin/press',
            'icon' => 'far fa-fw fa-newspaper',
            'active' => [
                'regex:@^admin/press(/[0-9]+/edit)?(/create)?$@'
            ],
        ],
        [
            'text' => 'Videos',
            'url'  => 'admin/youtube_videos',
            'icon' => 'fab fa-fw fa-youtube',
            'active' => [
                'regex:@^admin/youtube_videos(/[0-9]+/edit)?(/create)?$@'
            ],
        ],
        [
            'text' => 'Video Categories',
            'url'  => 'admin/video_categories',
            'icon' => 'fas fa-fw fa-box-open',
            'active' => [
                'regex:@^admin/video_categories(/[0-9]+/edit)?(/create)?$@'
            ],
        ],

        ['header' => 'LOCATIONS'],
        [
            'text' => 'Districts',
            'url'  => 'admin/districts',
            'icon' => 'fas fa-fw fa-road',
            'active' => [
                'regex:@^admin/districts(/[0-9]+/edit)?(/create)?$@'
            ],
        ],
        [
            'text' => 'District Zones',
            'url'  => 'admin/district_zones',
            'icon' => 'fas fa-fw fa-border-none',
            'active' => [
                'regex:@^admin/district_zones(/[0-9]+/edit)?(/create)?$@'
            ],
        ],


        ['header' => 'SOCIAL & TEAM'],
        [
            'text' => 'Testimonial',
            'url'  => 'admin/testimonials',
            'icon' => 'far fa-fw fa-comments',
            'active' => [
                'regex:@^admin/testimonials(/[0-9]+/edit)?(/create)?$@'
            ],
        ],
        [
            'text' => 'Subscribers',
            'url'  => 'admin/newsletters/subscribers',
            'icon' => 'fas fa-fw fa-mail-bulk',
            'active' => [
                'regex:@^admin/newsletters/subscribers(/[0-9]+/edit)?(/create)?$@'
            ],
        ],
        [
            'text' => 'Team Members',
            'url'  => 'admin/team_members',
            'icon' => 'fas fa-fw fa-user-friends',
            'active' => [
                'regex:@^admin/team_members(/[0-9]+/edit)?(/create)?$@'
            ],
        ],





        ['header' => 'SETTINGS'],
        [
            'text' => 'My Profile',
            'url'  => 'admin/profile',
            'icon' => 'fas fa-fw fa-user',
        ],

        ['header' => 'System'],
        [
            'text' => 'Users',
            'url'  => 'admin/system/users',
            'icon' => 'fas fa-fw fa-users-cog',
            'active' => [
                'regex:@^admin/system/users(/[0-9]+/edit)?(/create)?$@'
            ],
        ],
        [
            'text' => 'Configuration',
            'url'  => 'admin/system/config',
            'icon' => 'fas fa-fw fa-cogs',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SearchFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\LangFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Configure which JavaScript plugins should be included. At this moment,
    | DataTables, Select2, Chartjs and SweetAlert are added out-of-the-box,
    | including the Javascript and CSS files from a CDN via script and link tag.
    | Plugin Name, active status and files array (even empty) are required.
    | Files, when added, need to have type (js or css), asset (true or false) and location (string).
    | When asset is set to true, the location will be output using asset() function.
    |
    */

    'plugins' => [
        [
            'name' => 'Select2',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css',
                ],
            ],
        ],
        [
            'name' => 'jquery-typeahead',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/jquery-typeahead/2.10.6/jquery.typeahead.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/jquery-typeahead/2.10.6/jquery.typeahead.min.css',
                ],
            ],
        ],
        [
            'name' => 'chosen',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css',
                ],
            ],
        ],
        [
            'name' => 'bootstrap-toggle',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css',
                ],
            ],
        ],
        [
            'name' => 'alertify',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/alertify.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/css/alertify.min.css',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/css/themes/default.min.css',
                ],
            ],
        ],
        [
            'name' => 'toastr',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css',
                ],
            ],
        ],
        [
            'name' => 'momentjs',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js',
                ],

            ],
        ],
        [
            'name' => 'bootstrap-datetimepicker',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css',
                ],
            ],
        ],

        [
            'name' => 'summernote',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css',
                ],
            ],
        ],
        [
            'name' => 'Chartjs',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js',
                ],
            ],
        ],
        [
            'name' => 'Sweetalert2',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//unpkg.com/sweetalert/dist/sweetalert.min.js',
                ],
            ],
        ],
        [
            'name' => 'Pace',
            'active' => true,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-minimal.min.css',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js',
                ],
            ],
        ],
    ],
];
