<?php

return [

    /*
    |--------------------------------------------------------------------------
    | HTTP Clients
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for http clients used by the app.
    |
    | When a key is provided in the config, there will be set an Authorization
    | header of type "Bearer <key>"
    */

    'emag' => [
        'url' => env('EMAG_URL'),
        'timeout' => env('REBS_TIMEOUT', 60)
    ]

];
