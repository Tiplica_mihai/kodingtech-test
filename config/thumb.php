<?php

return [
    'images_path' => 'storage/media/img',
    'images_path_cache' => 'storage/media/img/properties',
];
