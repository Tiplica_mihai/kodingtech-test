CREATE USER 'kodingtech'@'localhost' IDENTIFIED BY 'kodingtech';
CREATE DATABASE `kodingtech`;
GRANT ALL PRIVILEGES ON `kodingtech`.* TO 'kodingtech'@'localhost';
FLUSH PRIVILEGES;
