<?php

use App\Models\Config;
use Illuminate\Database\Seeder;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Config::KEYS_MAPPING as $key => $type) {
            $config = Config::where('key', $key)->first();
            if (!$config) {
                Config::create([
                    'key' => $key,
                    'value' => null,
                    'type' => $type,
                ]);
            }
        }
    }
}
