<?php

use App\Models\Config;
use App\Models\Management\TeamMember;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;

class InitialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Auth::loginUsingId(1);
        $this->seedConfig();
    }

    private function seedConfig()
    {
        $configs = [
            Config::KEY_SOCIAL_FACEBOOK => 'https://www.facebook.com/',
            Config::KEY_SOCIAL_YOUTUBE => 'https://www.youtube.com/',
            Config::KEY_SOCIAL_INSTAGRAM => 'https://www.instagram.com/',
            Config::KEY_CONTACT_FORM_EMAIL => 'office@greenalley.ro',
            Config::KEY_CONTACT_FORM_PHONE => '0742 555 666',
            Config::KEY_PROPERTIES_BCC_EMAIL => 'office@greenalley.ro',
            Config::KEY_FACEBOOK_PIXEL => '1024185494601979',
        ];

        foreach ($configs  as $key => $value) {
            Config::where('key', $key)->update(['value' => $value]);
        }
    }
}
