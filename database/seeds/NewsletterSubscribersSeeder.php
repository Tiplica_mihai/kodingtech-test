<?php

use App\Models\Newsletter\Subscriber;
use Illuminate\Database\Seeder;

class NewsletterSubscribersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Subscriber::count()) {
            factory(Subscriber::class, 100)->create();
        }
    }
}
