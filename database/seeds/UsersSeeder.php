<?php

use Illuminate\Database\Seeder;
use App\Models\Management\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (User::count() == 0) {
            User::create([
                'name' => 'DudeX Dude',
                'email' => 'dudex992@gmail.com',
                'password' => 'ThisIsAPassword',
            ]);

            User::create([
                'name' => 'Alina Verdeata',
                'email' => 'alina.verdeata@greenalley.ro',
                'password' => 'Alina.$',
            ]);
        }

        factory(User::class, 20)->create();
    }
}
