/* global alertify, toastr */

'use strict';

const DASHBOARD_GRAPH_OPTIONS = {
    showScale: true,
    scaleShowGridLines: true,
    scaleGridLineColor: 'rgba(100,100,100,.175)',
    scaleGridLineWidth: 1,
    scaleShowHorizontalLines: true,
    scaleShowVerticalLines: false,
    bezierCurve: true,
    bezierCurveTension: 0.3,
    pointDot: true,
    pointDotRadius: 3,
    pointDotStrokeWidth: 0,
    pointHitDetectionRadius: 20,
    datasetStroke: true,
    datasetStrokeWidth: 2,
    datasetFill: true,
    maintainAspectRatio: true,
    responsive: true
};

function initChosen() {
    var config = {
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "100%"}
    };

    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
}

var datetimepickerProperties = {
    format: 'DD-MM-YYYY HH:mm',
    sideBySide: true
};


var deleteResource = function (title, message, id) {
    alertify.confirm(title, message,
            function () {
                $('#' + id).submit();
            },
            function () {
                toastr['error']('Request canceled');
            });
};

var deleteWithReasonResource = function (title, message, id) {
    alertify.prompt(title, message, null, function (evt, value) {
        $('#' + id + ' input[name=delete_reason]').val(value);
        $('#' + id).submit();
    }, function () {
        toastr['error']('Request canceled');
    });
};

$(document).ready(function () {
    $(".chosen").chosen({
        '.chosen-select': {width: "100%"},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "100%"}
    });
});
