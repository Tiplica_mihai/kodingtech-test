<?php
/* EN */

return [
    'panel' => [
        'search' => [
            'title' => 'Filter Results',
            'btn' => [
                'search' => 'Search',
                'reset' => 'Reset Fields',
            ]
        ]
    ],
    'table' => [
        'no_results' => 'We couldn\'t find any result',
    ],
    'search' => 'Fast Search',
    'boolean' => [
        1 => 'YES',
        0 => 'NO',
    ],
    'inputs' => [
        'select' => [
            'option' => 'Select Option',
        ],
        'date_range_pricker' => [
            'range' => [
                'today' => 'Today',
                'yesterday' => 'Yesterday',
                'last_7' => 'Last 7 Days',
                'last_30' => 'Last 30 Days',
                'this_month' => 'This Month',
                'last_month' => 'Last Month',
            ],
            'btn' => [
                'ok' => 'OK',
                'cancel' => 'Cancel',
                'custom' => 'Custom',
            ],
        ],
    ],
    'calendar' => [
        'days' => [
            'monday' => 'Monday',
            'tuesday' => 'Tuesday',
            'wednesday' => 'Wednesday',
            'thursday' => 'Thursday',
            'friday' => 'Friday',
            'saturday' => 'Saturday',
            'sunday' => 'Sunday',
        ],
        'days_int' => [
            'short' => [
                0 => 'Sun',
                1 => 'Mon',
                2 => 'Tue',
                3 => 'Wen',
                4 => 'Thu',
                5 => 'Fri',
                6 => 'Sat',
            ],
            'long' => [
                0 => 'Sun',
                1 => 'Mon',
                2 => 'Tue',
                3 => 'Wen',
                4 => 'Thu',
                5 => 'Fri',
                6 => 'Sat',
            ],
        ],
    ],
    'btn' => [
        'generate' => '<i class="fas fa-file-download"></i> Generate',
        'back_action' => 'Back',
        'filters' => 'Search',
    ],
    'attributes' => [
        'actions' => 'Actions',
        'created_at' => 'Created At',
        'updated_at' => 'Updated At',
        'deleted_at' => 'Deleted At',
        'creator_name' => 'Created By',
    ],
];
