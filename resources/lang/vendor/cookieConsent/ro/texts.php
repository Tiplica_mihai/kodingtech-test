<?php

return [
    'message' => 'Utilizăm cookie-urile pentru a vă asigura că vă oferim 
    cea mai bună experiență pe site-ul nostru. Dacă veți continua să 
    utilizați acest site, sunteți de acord cu acestea.
    Găsiți mai multe informații accesând pagina ',
    'agree' => 'Accept',
    'page' => 'Politica de Confidențialitate',
];
