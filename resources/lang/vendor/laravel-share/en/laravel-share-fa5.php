<?php

return [
    'facebook'  => '<li><a href=":url" aria-label="Facebook" class="social-button facebook-bg :class" id=":id"><i class="fab fa-facebook-f"></i></a></li>',
    'twitter'   => '<li><a href=":url" aria-label="Twitter" class="social-button twitter-bg :class" id=":id"><i class="fab fa-twitter"></i></a></li>',
    'linkedin'  => '<li><a href=":url" aria-label="Linkedin" class="social-button linkedin-bg :class" id=":id"><i class="fab fa-linkedin-in"></i></a></li>',
    'pinterest' => '<li><a href=":url" aria-label="Pinterest" class="social-button :class" id=":id"><i class="fab fa-pinterest"></i></a></li>',
    'whatsapp'  => '<li><a target="_blank" rel="noreferrer" href=":url" aria-label="Whatsapp" class="social-button whatsapp-bg :class" id=":id"><i class="fab fa-whatsapp"></i></a></li>',
    'telegram'  => '<li><a target="_blank" rel="noreferrer" href=":url" aria-label="Telegram" class="social-button telegram-bg :class" id=":id"><i class="fab fa-telegram-plane"></i></a></li>',
    'reddit'    => '<li><a target="_blank" rel="noreferrer" href=":url" aria-label="Reddit" class="social-button :class" id=":id"><i class="fab fa-reddit"></i></a></li>',
];
