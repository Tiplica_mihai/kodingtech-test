<a class='btn btn-flat btn-xs {{ $state }} btn-action' title='{{ $title }}' href='{{ route($route, $id) }}'
   onclick='event.preventDefault(); deleteResource("{{ $title }}", "{{ $message }}", "{{ "$formId-$realId" }}")'>
    <i class='fa {{ $icon }}'></i>
</a>

@push('js')
{!! Form::open(['url' => route($route, $id), 'method' => 'DELETE', 'id' => "$formId-$realId", 'class' => 'hidden']) !!}
{!! Form::close() !!}
@endpush
