<a href='{{ route($route, $id) }}' class="pull-right btn-box-tool" @tooltip( {{ $title }} )
   onclick='event.preventDefault(); deleteResource("{{ $title }}", "{{ $message }}", "{{ "$formId-$realId" }}")'>
    <i class='fa {{ $icon }}'></i></a>

@push('js')
{!! Form::open(['url' => route($route, $id), 'method' => 'DELETE', 'id' => "$formId-$realId", 'class' => 'hidden']) !!}
{!! Form::close() !!}
@endpush
