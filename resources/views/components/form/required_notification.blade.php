<div class="clearfix"></div>
<div class='box-footer'>
    {!! Form::appSubmit($cancelRoute, $okLabel, $cancelLabel) !!}
    <p class='text-right'>
        All fields with <span class='text-danger text-bold'>*</span> are mandatory!<br>
        @stack('extra_required')
    </p>
</div>
