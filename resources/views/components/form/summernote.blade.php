{!! Form::appOpenFormGroup($label, $name, $width, $properties) !!}
{!! Form::hidden($name, $value) !!}
<div class = 'summernote' id = '{{ $name }}'>{!! $old !!}</div>
{!! Form::appCloseFormGroup($name) !!}

@push('js')
<script>
    $(document).ready(function () {
        $('#{{ $name }}').summernote({
            height: 350,
            callbacks: {
                onChange: function (value) {
                    $('input[name="{{ $name }}"]').val(value);
                }
            }
        });
    });
</script>
@endpush
