<div class='control-label col-md-{{ $width }} col-sm-12 col-xs-12 less-padding'>
    <div class='form-group {{ $inputError ? 'has-error' : '' }}'>
        <label>{!! $label !!}
            @if($required)
            <span class="text-danger">*</span>
            @endif
        </label>
        @includeWhen(isset($properties['data-info-tooltip']) ,'components.form.info_tooltip',['info' => isset($properties['data-info-tooltip']) ? $properties['data-info-tooltip'] : ''])
        
        {!! Form::hidden($name . "_id", $value->id, ['id' => $name . "_id"]) !!}
        <div class='typeahead__container' id='{{ $name }}-container'>
            <div class='typeahead__field'>
                <span class='typeahead__query'>
                    {!! Form::text($name, $value->label, $required ? array_merge($properties, ['required' => '']) : $properties) !!}
                </span>
            </div>
        </div>

        <span class='help-block'>
            <strong>{!! $inputError !!}</strong>
        </span>
    </div>
</div>
<script>
    $(document).ready(function () {
        $.typeahead({
            input: '#{{ $name }}',
            minLength: 2,
            order: 'desc',
            hint: true,
            backdrop: {'background-color': '#ccc', 'z-index': '1000'},
            emptyTemplate: 'No result for @{{query}}',
            template: '<span style="font-size:1.2em">@{{name}}</span> <br><span class="text-info">@{{notes}}</span>',
            filter: false,
            dynamic: true,
            source: {
                results: {
                    display: ['name'],
                    ajax: {
                        type: 'GET',
                        url: '{!! $url !!}',
                        path: 'data.results',
                        data: {
                            search: '@{{query}}',
                            <?php echo $extraData ?>

                        },
                        headers: {'X-CSRF-TOKEN': window.csrfToken},
                        callback: {
                            fail: function (data) {
                                $.each(data.responseJSON.errors, function (index, data) {
                                    toastr["error"](data[0]);
                                });
                            }
                        }
                    }
                }
            },
            debug: true,
            callback: {
                onClick: function (node, a, item, event) {
                    $('#{{ $name . "_id" }}').val(item.id).trigger('change');
                    /*@if($callbackFunction)*/
                    {!! $callbackFunction !!}
                    /*@endif*/
                },
                onCancel: function (node, event) {
                    $('#{{ $name . "_id" }}').removeAttr('value').trigger('change');
                    /* @if($cancelFunction) */
                    {!! $cancelFunction !!}
                    /*@endif*/
                },
            }
        });
    });
</script>