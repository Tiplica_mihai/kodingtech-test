<a href="{{ $user->profileUrl }}" data-placement="top" data-toggle="tooltip" 
   data-original-title="{{ "{$user->name} ({$user->email})" }}">
    <img alt="member" class="img-circle " style="width: {{ $size }}px; height: {{ $size }}px;" src="{{ $user->avatarImg }}">
</a>