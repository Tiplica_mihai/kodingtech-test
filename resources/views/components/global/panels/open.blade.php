<section class="content"> 
    <div class="box {{ $class }}">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $title }}</h3></div>
        <div class="box-body">