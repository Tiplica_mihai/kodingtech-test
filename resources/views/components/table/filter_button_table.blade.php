<a class="pull-right btn-flat btn-xs btn-default" href="{{ orderTableLink($loop->index, $orderedContent) }}">

    @if(!isset(request()->order[$loop->index]))
    <i class="fa fa-sort"></i>
    @elseif(request()->order[$loop->index]['ascending'])
    <i class="fa fa-sort-asc"></i>
    @else
    <i class="fa fa-sort-desc"></i>
    @endif

</a>