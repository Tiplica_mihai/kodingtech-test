<table class='table table-hover table-bordered'>
    <thead>
        <tr>
            @if($hasNrCrt)
            <th style="width: 35px">#</th>
            @endif

            @foreach ($headers as $h) 
            @if($loop->last && $alignLastRight)
            <th class='text-right'>
                {!! $h !!}
                @includeWhen(isset($orderedContent[$loop->index]), 'components.table.filter_button_table')
            </th>
            @else 
            <th>
                {!! $h !!}
                @includeWhen(isset($orderedContent[$loop->index]), 'components.table.filter_button_table')
            </th>
            @endif
            @endforeach
        </tr>
    </thead>
    <tbody>
