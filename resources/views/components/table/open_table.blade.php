<div class='box-body table-responsive no-padding'>
    <div class='box-header'>
        <i class='fa fa-list'></i>
        <h3 class='box-title m-t-xs'> {!! $tableTitle !!}</h3>
        <div class="pull-right">
            @if($hasSearch)
                {!! Html::simpleSearch(request()->path(), 'q', __('general.search')) !!}
            @endif
        </div>
    </div>
    <div class='box-body'>
        @include('components.table.open_simple_table')
