<div class='input-group input-group-sm' style='width: 250px;'>
    {!! Form::open(['url' => $url, 'method' => $method]) !!}
    <div class='input-group'>
        {!! Form::text($name, request($name), ['placeholder' => $placeholder, 'class' => 'form-control input-sm']) !!}
        <span class='input-group-btn'>
            <button class='btn bg-primary btn-flat btn-sm' type='submit'>{!! $btnName !!}</button>
            <a href="{{ url($url) }}" class='btn btn-flat btn-danger btn-sm'><i class="fas fa-times"></i></a>
        </span>
    </div>
    {!! Form::close() !!}
</div>
