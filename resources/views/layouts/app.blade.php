<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layouts.components.meta-header')
        @include('layouts.components._styles')
    </head>
    <body>
        @include('layouts.components.header-main')

        @yield('content')

        @include('layouts.components._scripts')
    </body>
</html>
