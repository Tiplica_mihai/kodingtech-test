<!-- Global site tag (gtag.js) -->
<script async src='https://www.googletagmanager.com/gtag/js?id={!! Conf::googleAdWards() !!}'></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '{!! Conf::googleAdWards() !!}');
</script>
