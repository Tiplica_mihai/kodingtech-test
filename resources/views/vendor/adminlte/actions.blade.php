
<div class="col-xs-12 m-t-sm">
        @foreach($actions as $a)
        <a class="btn btn-sm btn-flat {{ isset($a['class']) ? $a['class'] : 'btn-success' }}"

            @isset($a['tooltip'])
            data-container='body' data-toggle='tooltip' data-placement='left' data-html='true' title="{{ $a['tooltip'] }}"
            @endisset

            href="{!! $a['href'] !!}">
            @isset($a['icon']) <i class="fas {{ $a['icon'] }} m-r-xs"></i> @endisset

            {!! $a['title'] !!}</a>
        @endforeach
</div>
<div class="clearfix"></div>

