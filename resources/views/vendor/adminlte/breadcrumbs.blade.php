<ol class="breadcrumb">
    @foreach($breadcrumbs as $b)

    @if($loop->last)
    <li class="active">
        @isset($b['icon'])

        @php
        $faType = (count(explode(' ', $b['icon'])) > 1 ? '' : 'fas');
        @endphp

        <i class="{{isset($b['icon']) ? "$faType $b[icon]" : 'far fa-circle'}}"></i>
        @endisset

        {!! $b['name'] !!}
    </li>
    @else
    <li>
        <a href="{{ $b['href'] }}">
            @isset($b['icon'])

            @php
            $faType = (count(explode(' ', $b['icon'])) > 1 ? '' : 'fas');
            @endphp

            <i class="{{isset($b['icon']) ? "$faType $b[icon]" : 'far fa-circle'}}"></i>
            @endisset

            {!! $b['name'] !!}
        </a>
    </li>
    @endif

    @endforeach
</ol>


