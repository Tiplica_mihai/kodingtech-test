<script>
    $(document).ready(function () {
        /*
         @foreach(['success', 'error'] as $msg_class)
         @foreach(request()->session()->get($msg_class, []) as $msg)
         */
        toastr['{{ $msg_class }}'](`{{ $msg }}`);
        /*
         @endforeach
         @endforeach

         @foreach ($errors->all() as $error)
         */
        toastr['error'](`{{ $error }}`);
        /*
         @endforeach
         */

    });
</script>
