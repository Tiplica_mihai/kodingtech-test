<div class="js-cookie-consent cookie-consent alert alert-dark alert-dismissible fade show  wow fadeInUp"  data-wow-delay="1000ms">
    <div class="container">
        <button type="button" class="close text-white" data-dismiss="alert">&times;</button>
        <br>
        <span class="cookie-consent__message pt-3">
            {!! trans('cookieConsent::texts.message') !!}
            <a href="{{ route('web.confidentiality-policy') }}" target="_blank" class="badge badge-light">
                {!! trans('cookieConsent::texts.page') !!}</a>
        </span>
        <br>

        <button class="js-cookie-consent-agree cookie-consent__agree btn btn-outline-success bomd mt-3">
            {{ trans('cookieConsent::texts.agree') }}
        </button>
    </div>
</div>
