<form>
    <legend>Filtre</legend>
    <div class="form-group">
        <label>Nume</label>
        <input type="text" name="name" class="form-control" value="{{ request('name') }}">
    </div>
    <div class="form-group">
        <label>SKU</label>
        <input type="text" name="part_number" class="form-control" value="{{ request('part_number') }}">
    </div>
    <div class="form-group">
        <label>Pret Min</label>
        <input type="text" name="min_price" class="form-control" value="{{ request('min_price') }}">
    </div>
    <div class="form-group">
        <label>Pret Max</label>
        <input type="text" name="max_price" class="form-control" value="{{ request('max_price') }}">
    </div>
    <button type="submit" class="btn btn-primary">Cauta</button>
    <a href="{{ route('web.home')}}" class="btn btn-warning">Reseteaza</a>
</form>