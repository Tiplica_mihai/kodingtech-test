<div class="col-md-4 my-3">
    <div class="card">
        <img src="{{ $product->image_url }}" class="card-img-top" alt="{{ $product->name }}">
        <div class="card-body">
            <h5 class="card-title">{{ $product->name }}</h5>
            <p class="card-text">
                SKU: {{ $product->part_number }}
                <br>
                Pret: {{ $product->price }} LEI
            </p>
            <a href="#" class="btn btn-primary">Cumpara</a>
        </div>
    </div>
</div>