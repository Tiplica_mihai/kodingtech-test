@extends(BLADE_LAYOUT_WEB)

@section('content')


<div class="container">
    <div class="row">
        <div class="col-3">
            @include('web.home.components.filters')
        </div>
        <div class="col-9">
            <div class="row">
                @each( 'web.home.components.product', $products, 'product')
                <div class="col-12">{{ $products->appends(request()->except('page'))->render() }}</div>
            </div>
        </div>
    </div>
</div>

@endsection